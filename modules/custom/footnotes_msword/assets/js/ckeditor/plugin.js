﻿/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

/**
 * @fileOverview Paste as plain text plugin.
 */

( function() {


	CKEDITOR.plugins.add('footnotes_msword',
	{
		requires: ['htmlwriter'],
		init: function(editor) {
			editor.on('paste', function(evt)
			{
				
				/*
				var whitelist = "p"; // for more tags use the multiple selector, e.g. "p, img"
$(evt.data.dataValue).not(whitelist).each(function() {
    var content = $(this).contents();
    $(this).replaceWith(content);
	alert ("Texttest");
});
evt.data.dataValue = evt.data.dataValue.replace(/<h1>/g, "<p>").replace(/<\/h1>/g, "</p>").replace(/<i>/g, "<em>").replace(/<\/i>/g, "</em>").replace(/(?:\s|&nbsp;)(?:\s|&nbsp;)+/g, " ");
evt.data.dataValue = strip_tags(evt.data.dataValue , '<a><blockquote><em><fn><h2><p><u><hoptext><constituency><constdata>');
*/

evt.data.dataValue = evt.data.dataValue.replace(/<h1>/g, "<p>").replace(/<\/h1>/g, "</p>");

			// Split the main body from the footnotes
				var parts = evt.data.dataValue.split(/(.*)?(<div>.*)/img);

				if(parts.length == 1) {
					evt.data.dataValue = evt.data.dataValue.replace(/<i>/g, "<em>").replace(/<\/i>/g, "</em>").replace(/(?:\s|&nbsp;)(?:\s|&nbsp;)+/g, " ");
					evt.data.dataValue = strip_tags(evt.data.dataValue , '<a><blockquote><em><fn><h2><p><u><hoptext><constituency><constdata>');
					editor.setData( evt.data.dataValue);
					return;
				}
				//  To do: add edn tag support next to ftn  tag..
				var	output = parts[1],
					tokens = parts[2].split(/<div id="(edn[\d]+)">.*?<\/a>(.*?)(<\/p>|<\/div>)/img),
					length = tokens.length - 1,
					index = 1,
					pattern = '',
					replacement = '';

				for(index; index < length; index++) {

					if (tokens[index].indexOf('edn') == 0) {
						pattern = new RegExp('<a.*?#_' + tokens[index] + '".*?<\/a>', 'g');
					    // Cleaning of any footnotes text 
						replacement = '<fn>' + strip_tags('<p>' + tokens[index+1].replace(/(&nbsp;)/img, '') + '</p>','<u>') + '</fn>';
						output = output.replace(pattern, replacement);
					}
				}
				
				output = output.replace(/<i>/g, "<em>").replace(/<\/i>/g, "</em>").replace(/(?:\s|&nbsp;)(?:\s|&nbsp;)+/g, " ");
				output = strip_tags(output , '<a><blockquote><em><fn><h2><p><u><hoptext><constituency><constdata>');

				editor.setData(output);
			});
		}
	});
	
	
	
} )();

function strip_tags(str, allow) {
  // making sure the allow arg is a string containing only tags in lowercase (<a><b><c>)
  allow = (((allow || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');

  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
  var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return str.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
    return allow.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
  });
}

/**
 * Whether to force all pasting operations to insert on plain text into the
 * editor, loosing any formatting information possibly available in the source
 * text.
 *
 * **Note:** paste from word (dialog) is not affected by this configuration.
 *
 *		config.forcePasteAsPlainText = true;
 sadsad asd
 *
 * @cfg {Boolean} [forcePasteAsPlainText=false]
 * @member CKEDITOR.config
 */
