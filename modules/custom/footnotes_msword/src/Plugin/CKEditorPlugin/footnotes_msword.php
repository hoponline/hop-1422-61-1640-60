<?php

/**
 * @file
 * Contains \Drupal\footnotes_msword\Plugin\CKEditorPlugin\footnotes_msword.
 */

 namespace Drupal\footnotes_msword\Plugin\CKEditorPlugin;
 
  use Drupal\ckeditor\CKEditorPluginBase;
  use Drupal\ckeditor\CKEditorPluginContextualInterface;
  use Drupal\editor\Entity\Editor;

  /**
  * Defines a "footnotes_msword" plugin, with a contextually enabled "llama" feature.
  *
  * @CKEditorPlugin(
  *   id = "footnotes_msword",
  *   label = @Translation("Footnotes MSword")
  * )
  */
  class footnotes_msword extends CKEditorPluginBase implements CKEditorPluginContextualInterface {

  /**
  * {@inheritdoc}
  */
  function isEnabled(Editor $editor) {
    // Automatically enable this plugin if the Paste button is enabled.
     $settings = $editor->getSettings();
     foreach ($settings['toolbar']['rows'] as $row) {
       foreach ($row as $group) {
          if (in_array('Paste', $group['items'])) {
            return TRUE;
          }
        }
      }
      return FALSE;
    }

    /**
     * {@inheritdoc}
     */
    function getFile() {
     return drupal_get_path('module', 'footnotes_msword') . '/assets/js/ckeditor/plugin.js'; 
    }

	
	  /**
   * {@inheritdoc}
   *
   * NOTE: The keys of the returned array corresponds to the CKEditor button
   * names. They are the first argument of the editor.ui.addButton() or
   * editor.ui.addRichCombo() functions in the plugin.js file.
   */
  public function getButtons() {
    // Make sure that the path to the image matches the file structure of
    // the CKEditor plugin you are implementing.
     return array();
  }

  
  /**
   * {@inheritdoc}
   */
  function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  function getDependencies(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  function getLibraries(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }
  
    }

	

	
	